import { useState,useEffect, Fragment } from "react";
import { useParams } from "react-router-dom";
import ProductCard from "../../components/product-card";
import "./category.styles.scss"
import { useSelector } from "react-redux";
import { getCategoriesMap, IsCategoriesLoading } from "../../store/categories/selector";
import Spinner from "../../components/spinner";


const Category =()=>{
    const {category}=useParams();
    const isCategoriesLoading=useSelector(IsCategoriesLoading);
    const products=useSelector(getCategoriesMap);
    const [categoryProducts, setCategoryProducts]=useState();

    useEffect(()=>{
        setCategoryProducts(products[category]);
    },[category,products])

    return(
        <Fragment>
            <div className="category-title">{category.toUpperCase()}</div>
            <div className="category-container">
                {
                    isCategoriesLoading ?<Spinner/>:
                    (
                        categoryProducts && categoryProducts.map(product=>(
                            <ProductCard key={product.id} product={product}/>
                        ))
                    )
                }
                
            </div>
        </Fragment>
    )

}

export default Category;