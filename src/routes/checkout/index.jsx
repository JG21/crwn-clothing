import { useSelector } from "react-redux";
import { selectCartItemTotal,selectCartItems } from "../../store/cart/selector";
import CheckoutItem from "../../components/checkout-item";
import "./checkout.styles.scss"

const Checkout=()=>{
   const cartItems= useSelector(selectCartItems);
   const cartTotal=useSelector(selectCartItemTotal);
  return(
    <div className="checkout-container">
        <div className="checkout-header">
          <div className="header-block">
            <span>Product</span>
          </div>
          <div className="header-block">
            <span>Description</span>
          </div>
          <div className="header-block">
            <span>Quantity</span>
          </div>
          <div className="header-block">
            <span>Price</span>
          </div>
          <div className="header-block">
            <span>Remove</span>
          </div>
        </div>
        
        {cartItems.map( (item) => (
            <CheckoutItem key={item.id} cartItem={item}/>
            ))}
        
        <div className="total">Total : £{cartTotal}</div>
    </div>
  )
}

export default Checkout;