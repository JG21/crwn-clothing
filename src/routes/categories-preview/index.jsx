import { Fragment } from "react";
import { useSelector } from "react-redux";
import CategoryPreview from "../../components/category-preview";
import { getCategoriesMap } from "../../store/categories/selector";

const CategoriesPreview =()=>{
 const products= useSelector(getCategoriesMap)
 return(
      <Fragment>
       {
         Object.keys(products).map(title=>{
            const catrgoryProducts= products[title];
            return <CategoryPreview key={title} title={title} products={catrgoryProducts} />
            })}
      </Fragment>
 )
}

export default CategoriesPreview;