import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Route, Routes } from "react-router-dom";
import {fetchCategoriesStart} from "../../store/categories/action";
import CategoriesPreview from "../categories-preview";
import Category from "../category";

import './shop.styles.scss';

const Shop =()=>{
   const dispatch=useDispatch();
   useEffect(()=>{
      dispatch(fetchCategoriesStart());
  },[]) 

 return(
      <Routes>
         <Route index element={<CategoriesPreview/>}/>
         <Route path=":category" element={<Category/>}/>
      </Routes>
 )
}

export default Shop;