import SignUPForm from "../../components/sign-up-form"
import SignInForm from "../../components/sign-in-form"
import "./authentication.styles.scss";

const Authentication=()=>{
    return(
        <div className="authentication">
            <SignInForm/>
            <SignUPForm/>           
        </div>
    )
}

export default Authentication;
