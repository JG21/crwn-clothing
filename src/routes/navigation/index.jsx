import { useSelector } from "react-redux";
import { Outlet } from "react-router-dom";
import { useDispatch } from "react-redux";
import {ReactComponent as CrwnLogo} from "../../assets/crown.svg";
import { signOutStart } from "../../store/user/action";
import CartIcon from "../../components/cart-icon";
import CartDropdown from "../../components/cart-dropdown";

import {NavigationContainer,LogoContainer,NavLinks,NavLink} from './navigation.styles';
import { selectCurrentUser } from "../../store/user/selector";
import { selectIsCartOpen } from "../../store/cart/selector";

const Navigation=()=>{
    const dispatch=useDispatch();
    const currentUser= useSelector(selectCurrentUser);
    const isCartOpen=useSelector(selectIsCartOpen); 

    const SignOutHandler = ()=>{
        dispatch(signOutStart());
    }

    return(
        <>
        <NavigationContainer>
            <LogoContainer to='/'>
                <CrwnLogo className='logo'/>
            </LogoContainer>       
            <NavLinks>
                <NavLink to='/shop'>
                    SHOP
                </NavLink>
                {
                  currentUser ? (<NavLink onClick={SignOutHandler}>
                  Sign Out </NavLink>) :
                  (<NavLink to='/auth'>
                  Sign In </NavLink>) 
                }
                <CartIcon/>
            </NavLinks>
            {isCartOpen && <CartDropdown/>}
        </NavigationContainer>
        <Outlet/>
        </>
    )
}

export default Navigation;