// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth, 
        signInWithPopup, 
        signInWithEmailAndPassword,
        GoogleAuthProvider,
        createUserWithEmailAndPassword,
        signOut,
        onAuthStateChanged} from "firebase/auth";
import {doc,
        getDoc,
        setDoc, 
        getFirestore,
        collection,
        writeBatch,
        query,
        getDocs} from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBh2AxzYKFAZqt3DEXchnOqbt77GTwCIaw",
  authDomain: "crwn-clothing-db-672ad.firebaseapp.com",
  projectId: "crwn-clothing-db-672ad",
  storageBucket: "crwn-clothing-db-672ad.appspot.com",
  messagingSenderId: "609346662281",
  appId: "1:609346662281:web:afe8b952493a709172bb58"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);

const googleProvider= new GoogleAuthProvider();
googleProvider.setCustomParameters({
    prompt:"select_account"
});

export const auth= getAuth();
export const signInWithGooglePopup=()=> signInWithPopup(auth, googleProvider);

const db=getFirestore();

export const addCollectionAndDocuments= async(collectionKey, objectToAdd)=>{
  const collectionRef=collection(db, collectionKey);
  
  const batch= writeBatch(db);
  objectToAdd.forEach(object => {
    const docRef= doc(collectionRef,object.title.toLowerCase());
    batch.set(docRef, object);
  });
 
  batch.commit();
  console.log("done");
}

export const getCategoriesAndDocuments= async ()=>{
  const collectionRef= collection(db, 'categories')
  const q= query(collectionRef);
  // await Promise.reject(new Error("Test error for thunk implimentation"));
  const querySnapshot= await getDocs(q);
  // const categoryMap= querySnapshot.docs.reduce((acc, docSnapShot) => {
  //     const {title, items}= docSnapShot.data();
  //     acc[title.toLowerCase()]=items;
  //     return acc;
  // },{});
  console.log(querySnapshot);
  return querySnapshot.docs.map(docSnapShot=>docSnapShot.data());
  //return categoryMap;
}

export const createUserDocumentFromAuth= async(userAuth, additionalInfo={})=>{
    const userDocRef=doc(db,'user',userAuth.uid)

    const userSnapshot= await getDoc(userDocRef);
    if(!userSnapshot.exists()){
       const {displayName,email}=userAuth; 
       const createdAt= new Date();
      try{
       setDoc(userDocRef,{
         displayName, 
         email, 
         createdAt,
         ...additionalInfo  
       });
      }catch(error){
         console.log('error creating the user', error.message)
      }
    }
  return userSnapshot;
};

export const createAuthUserWithEmailAndPassword= async (email, password)=>{
  if(!email || !password) return;  
  return await createUserWithEmailAndPassword(auth, email,password)
};

export const signInAuthUserWithEmailAndPassword= async (email, password)=>{
    if(!email || !password) return;  
    return await signInWithEmailAndPassword(auth, email,password)
  };

  export const signOutAuth= async ()=> await signOut(auth);

  export const onAuthStateChangedListner= (callback)=> onAuthStateChanged(auth, callback);

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubcribe = onAuthStateChanged(
      auth,
      (userAuth) => {
        unsubcribe();
        resolve(userAuth);
      },
      reject
    );
  });
};