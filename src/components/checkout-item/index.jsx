import { useDispatch,useSelector } from 'react-redux';
import {
    CheckoutItemContainer,
    ImageContainer,
    Image,
    SpanWidth,
    Quantity,
    RemoveButton
} from './checkout-item.styles'
import {clearItemfromCart, addItemToCart, removeItemFromCart} from "../../store/cart/action";
import { selectCartItems } from '../../store/cart/selector';

const CheckoutItem=({cartItem})=>{
    const dispatch=useDispatch();
    const previousCartItems=useSelector(selectCartItems)
    const{name, imageUrl, quantity, price} = cartItem; 
    const clearCartItemHandler=()=>dispatch(clearItemfromCart(previousCartItems,cartItem));
    const addItemToCartHandler=()=>dispatch(addItemToCart(previousCartItems,cartItem));
    const removeItemToCartHandler=()=>dispatch(removeItemFromCart(previousCartItems,cartItem));
    return(
        <CheckoutItemContainer>
            <ImageContainer>
                <Image src={imageUrl} alt={`${name}`}/>
            </ImageContainer>
            <SpanWidth>{name}</SpanWidth>
            <Quantity>
                <div className='arrow' onClick={removeItemToCartHandler}>
                 &#10094;
                </div>
                <span className='value'>{quantity}</span>
                <div className='arrow' onClick={addItemToCartHandler}>
                &#10095;   
                </div>
            </Quantity>
            <SpanWidth>{price}</SpanWidth>
            <RemoveButton onClick={clearCartItemHandler}>&#10005;</RemoveButton>
        </CheckoutItemContainer>
    )
}

export default CheckoutItem;