import { useDispatch, useSelector } from "react-redux";
import { setIsCartOpen} from "../../store/cart/action";
import { selectCartItems } from "../../store/cart/selector";
import { useNavigate } from "react-router-dom";
import Button from "../button";
import CartItem from "../cart-item";
import {CartDropdownContainer, CartItems, EmptyMessage} from "./cart-dropdown.styles";

const CartDropdown=()=>{
   const dispatch=useDispatch();
   const cartItems=useSelector(selectCartItems);
   const navigate=useNavigate();

   const checkoutHandler=()=>{
      navigate('/checkout')
      dispatch(setIsCartOpen(false))
   }
return(
    <CartDropdownContainer>
       <CartItems>
         {cartItems.length ? (cartItems.map(item => <CartItem key={item.id} cartItem={item}/>)) :
         <EmptyMessage>Your cart is empty </EmptyMessage>}
       </CartItems>
       <Button onClick={checkoutHandler} >GO TO CHECKOUT</Button>
    </CartDropdownContainer>
    
)
}

export default CartDropdown;