import { useState } from "react"
import { useDispatch } from "react-redux";
import { emailSignInStart, googleSignInStart } from "../../store/user/action";
import FormInput from "../form-input";
import Button, {BUTTON_TYPE_CLASSES} from '../button'
import './sign-in-form.styles.scss'

const SignInForm = ()=>{
 const dispatch=useDispatch();
 const defaultformField={
    email:'',
    password:''
 }
 const [formField, setFormField]=useState(defaultformField);
 const {email, password}=formField;

 const handleFormField=(event)=>{
   const{name, value}= event.target;
   setFormField({...formField, [name]:value})
 }

 const handleSubmit=async (event)=>{
    event.preventDefault();
   try{
    dispatch(emailSignInStart(email, password))
    setFormField(defaultformField); 
   }catch(error){
    switch(error.code){
        case 'auth/user-not-found':
              alert('email does not exist') 
              break;
        case 'auth/wrong-password': 
              alert('wrong password') 
              break;
        default:
            console.log(error)
    }
   }  
 }
 const signInWithGoogle= async ()=>{
    dispatch(googleSignInStart());
   }  
 
    return(
        <div className="sign-up-container">
            <h2> Already have an account?</h2>
            <span>sign in with your email and password</span>
            <form onSubmit={handleSubmit}>
                <FormInput
                lable="Email"
                required
                type='email'
                name='email'
                value={email}  
                onChange={handleFormField}   
                />
                <FormInput
                lable="Password"
                required
                type='password'
                name='password'
                value={password}  
                onChange={handleFormField}   
                />
                <div className='buttons-container'>
                    <Button type='submit'>Sign In</Button>
                    <Button
                         buttonType={BUTTON_TYPE_CLASSES.google}
                         type='button'
                         onClick={signInWithGoogle}
                    >
                     Sign In With Google
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default SignInForm;