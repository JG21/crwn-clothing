import { useSelector,useDispatch } from "react-redux";
import { selectIsCartOpen, selectCartItemCount } from "../../store/cart/selector";
import { setIsCartOpen } from "../../store/cart/action";
import {CartIconContainer, ShoppingIcon, ItemCount} from './cart-icon.styles';

const CartIcon=()=>{
   const dispatch=useDispatch();
   const isCartOpen= useSelector(selectIsCartOpen);
   const totalCartItem=useSelector(selectCartItemCount);
   const toggleIsCartOpen=()=>dispatch(setIsCartOpen(!isCartOpen)); 

   return(
    <CartIconContainer onClick={toggleIsCartOpen}>
        <ShoppingIcon/>
        <ItemCount>{totalCartItem}</ItemCount>
    </CartIconContainer>
   )
}

export default CartIcon;