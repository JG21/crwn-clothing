import { useState } from "react"
import { useDispatch } from "react-redux";
import { emailSignUpStart } from "../../store/user/action";
import FormInput from "../form-input";
import Button from '../button'
import './sign-up-form.styles.scss'

const SignUPForm = ()=>{
 const dispatch=useDispatch();   
 const defaultformField={
    displayName:'',
    email:'',
    password:'',
    confirmPassword:''
 }
 const [formField, setFormField]=useState(defaultformField);
 const {displayName, email, password,confirmPassword}=formField;

 const handleFormField=(event)=>{
   const{name, value}= event.target;
   setFormField({...formField, [name]:value})
 }

 const handleSubmit=async (event)=>{
    event.preventDefault();
   if(password !== confirmPassword) return;
   try{
    dispatch(emailSignUpStart(email,password, displayName))
//    const response= await createAuthUserWithEmailAndPassword(email,password)
//    await createUserDocumentFromAuth(response?.user, {displayName})
   setFormField(defaultformField); 
   }catch(error){
    if(error.code === 'auth/email-already-in-use'){
        alert('email already in use');
    }else{
    console.log('problem in user sign in', error.message)
    }
   }  
 }
 
    return(
        <div className="sign-up-container">
            <h2> Don't have an account?</h2>
            <span>sign up with your email and password</span>
            <form onSubmit={handleSubmit}>
                <FormInput
                lable="Name"
                required
                type='text'
                name='displayName'
                value={displayName}  
                onChange={handleFormField}   
                />
                <FormInput
                lable="Email"
                required
                type='email'
                name='email'
                value={email}  
                onChange={handleFormField}   
                />
                <FormInput
                lable="Password"
                required
                type='password'
                name='password'
                value={password}  
                onChange={handleFormField}   
                />
                <FormInput
                lable="Confirm Password"
                required
                type='password'
                name='confirmPassword'
                value={confirmPassword}  
                onChange={handleFormField}   
                />
                <Button type='submit'>Submit</Button>
            </form>
        </div>
    )
}

export default SignUPForm;