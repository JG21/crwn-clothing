import { categories } from "./categories";
import DirectoryItem from "../directory-items";
import {DirectoryContainer} from './directory.style'

const Directory = () => {
    return (
        <DirectoryContainer>
            {categories.map((category) => (
            <DirectoryItem category={category} key={category.id}/>
            ))}
        </DirectoryContainer>
    )
}

export default Directory;