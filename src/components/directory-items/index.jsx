import { useNavigate } from 'react-router-dom';
import {DirectoryItemContainer, BackgroungImage, Body} from './directory-items.style'

const DirectoryItem = ({ category }) => {
    const { title, imageUrl } = category;
    const navigate= useNavigate();

    const navigateHandler=()=>{
        const route=`/shop/${title}`;
       return navigate(route);
    }
    return (
     <DirectoryItemContainer onClick={navigateHandler}>
            <BackgroungImage imageUrl={imageUrl}/>
            <Body>
                <h2>{title}</h2>
                <p>Shop Now</p>
            </Body>
     </DirectoryItemContainer>  
    )
}
export default DirectoryItem;