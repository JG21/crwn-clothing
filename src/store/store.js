import {compose, createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
//import thunk from 'redux-thunk';
import createSagaMiddleware from '@redux-saga/core';
import { rootReducer } from './rootReducer';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { rootSaga } from './root-saga';

const sagaMiddleware = createSagaMiddleware();

const middleWare = [logger, sagaMiddleware];

const composeEnhancer = (window && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose; 

const composedEnhancers= composeEnhancer(applyMiddleware(...middleWare))

const persistConfig={
    key:'root',
    storage,
    whitelist: ['cart']
}

const persistedReducer=persistReducer(persistConfig, rootReducer)

export const store= createStore(persistedReducer, undefined,composedEnhancers )

sagaMiddleware.run(rootSaga);

export const persistedStore=persistStore(store);