import { takeLatest, call, all, put } from "redux-saga/effects";
import {
    getCurrentUser,
    createUserDocumentFromAuth,
    signInWithGooglePopup,
    signInAuthUserWithEmailAndPassword,
    createAuthUserWithEmailAndPassword,
    signOutAuth
} from "../../utils/firebase";
import { signInSuccess, signInFailure, emailSignUpSucess, emailSignUpFailure, signOutSuccess, signOutFailure } from "./action";
import { USER_ACTION_TYPES } from "./types";


export function* getSnapshotFromUserAuth(userAuth, additionalInfo) {
    try {
        const userSnapshot = yield call(createUserDocumentFromAuth, userAuth, additionalInfo);
        yield put(signInSuccess({ id: userSnapshot.id, ...userSnapshot.data() }))
    } catch (error) {
        yield put(signInFailure(error));
    }
}

export function* isUserAuthenticated() {
    try {
        const userAuth = yield call(getCurrentUser);
        if (!userAuth) return;
        yield call(getSnapshotFromUserAuth, userAuth);
    } catch (error) {
        yield put(signInFailure(error));
    }
}

export function* googleSignInWithPopup() {
    try {
        const { user } = yield call(signInWithGooglePopup);
        yield call(getSnapshotFromUserAuth, user);
    } catch (error) {
        yield put(signInFailure(error));
    }
}

export function* signInWithEmailAndPassword({ payload: { email, password } }) {
    try {
        const { user } = yield call(signInAuthUserWithEmailAndPassword, email, password);
        yield call(getSnapshotFromUserAuth, user);
    } catch (error) {
        yield put(signInFailure(error));
    }
}

export function* signUpWithEmailAndPassword({ payload: { email, password, displayName } }) {
    try {
        const { user } = yield call(createAuthUserWithEmailAndPassword, email, password);
        yield put(emailSignUpSucess(user, { displayName }))
    } catch (error) {
        yield put(emailSignUpFailure(error));
    }
}

export function* signUpWithEmailSuccess({ payload: { user, additionalInfo } }) {
    yield call(getSnapshotFromUserAuth, user, additionalInfo);
}

export function* signOut() {
    try {
        yield call(signOutAuth);
        yield put(signOutSuccess());
    } catch (error) {
        yield put(signOutFailure(error));
    }
}

export function* onGoogleSignInStart() {
    yield takeLatest(USER_ACTION_TYPES.GOOGLE_SIGN_IN_START, googleSignInWithPopup);
}

export function* onEmailSighInStart() {
    yield takeLatest(USER_ACTION_TYPES.EMAIL_SIGHN_IN_START, signInWithEmailAndPassword);
}

export function* onCheckUserSession() {
    yield takeLatest(USER_ACTION_TYPES.CHECK_USER_SESSION, isUserAuthenticated)
}

export function* onEmailSignUpStart() {
    yield takeLatest(USER_ACTION_TYPES.EMAIL_SIGN_UP_START, signUpWithEmailAndPassword)
}

export function* onEmailSignUpSuccess() {
    yield takeLatest(USER_ACTION_TYPES.EMAIL_SIGN_UP_SUCCESS, signUpWithEmailSuccess)
}

export function* onSignOutStart() {
    yield takeLatest(USER_ACTION_TYPES.SIGN_OUT_START, signOut)
}

export function* userSaga() {
    yield all([call(onCheckUserSession),
    call(onGoogleSignInStart),
    call(onEmailSighInStart),
    call(onEmailSignUpStart),
    call(onEmailSignUpSuccess),
    call(onSignOutStart),
    ])
}