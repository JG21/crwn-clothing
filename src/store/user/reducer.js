import { USER_ACTION_TYPES } from "./types";


const INITIAL_USER_STATE={
  currentUser: null,
  isLoading: false,
  error: null
}

export const userReducer=(state=INITIAL_USER_STATE, action)=>{
    const {type, payload}=action;
    switch(type){
      case USER_ACTION_TYPES.SIGN_IN_SUCCESS:
        return{
          ...state,
          isLoading: false,
          currentUser:payload,
        };
      case USER_ACTION_TYPES.SIGN_OUT_SUCESS:
        return {
          ...state,
          isLoading: false,
          currentUser: null,
        }
      case USER_ACTION_TYPES.SIGN_OUT_FAILURE:
      case USER_ACTION_TYPES.EMAIL_SIGN_UP_FAILURE:
      case USER_ACTION_TYPES.SIGN_IN_FAILURE:
        return {
          ...state,
          isLoading: false,
          error: payload,
        }
      default :
         return state;
    }
}