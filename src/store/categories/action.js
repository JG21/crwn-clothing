import { getCategoriesAndDocuments } from "../../utils/firebase";
import { createAction } from "../../utils/reducer";
import { CATEGORIES_ACTION_TYPES } from "./types";

export const setCategories=(categories)=>createAction(CATEGORIES_ACTION_TYPES.SET_CATEGORIES_MAP,categories);

export const fetchCategoriesStart = () => createAction(CATEGORIES_ACTION_TYPES.FETCH_CATOGORIES_SATRT);

export const fetchCategoriesSuccess = (categories) => createAction(CATEGORIES_ACTION_TYPES.FETCH_CATOGORIES_SUCCESS, categories);

export const fetchCategoriesFailure = (error) => createAction(CATEGORIES_ACTION_TYPES.FETCH_CATOGORIES_FAIL, error);


// export const fetchCategoriesAsync = () => async (dispatch) => {
//     dispatch(fetchCategoriesStart());
//     try {
//         const categories = await getCategoriesAndDocuments();
//         dispatch(fetchCategoriesSuccess(categories));
//     } catch (error) {
//         dispatch(fetchCategoriesFailure(error));
//     }
// }