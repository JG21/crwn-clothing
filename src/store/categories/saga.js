import { takeLatest, call, all, put } from 'redux-saga/effects';
import { fetchCategoriesSuccess, fetchCategoriesFailure } from './action';
import { CATEGORIES_ACTION_TYPES } from './types';
import { getCategoriesAndDocuments } from '../../utils/firebase';


export function* fetchCategoriesAsync() {
    try {
        const categories = yield call(getCategoriesAndDocuments);
        yield put(fetchCategoriesSuccess(categories));
    } catch (error) {
        yield put(fetchCategoriesFailure(error));
    }
}

export function* onFetchCategories() {
    yield takeLatest(CATEGORIES_ACTION_TYPES.FETCH_CATOGORIES_SATRT, fetchCategoriesAsync);
}

export function* categoriesSaga() {
    yield all([call(onFetchCategories)]);
}