import { CATEGORIES_ACTION_TYPES } from "./types";

const INITIAL_CATEGORIES_STATE={
    categories:[],
    isLoading: false,
    error: null
}

export const categoriesReducer = (state = INITIAL_CATEGORIES_STATE, action) => {
    const {type, payload}=action;
    switch(type){
        case CATEGORIES_ACTION_TYPES.FETCH_CATOGORIES_SATRT:
            return {
                ...state,
                isLoading: true,
            }
        case CATEGORIES_ACTION_TYPES.FETCH_CATOGORIES_SUCCESS:
            return{
                ...state,
                categories: payload,
                isLoading: false,
            }
        case CATEGORIES_ACTION_TYPES.FETCH_CATOGORIES_FAIL:
            return {
                ...state,
                error: payload,
                isLoading: false,
            }    
        default :
        return state;    
    }
}