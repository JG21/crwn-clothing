import {createSelector} from 'reselect';

const selectCategoryReducer=(state)=>state.categories;

const selectCategories=createSelector(
    [selectCategoryReducer],
    (categorySlice)=> categorySlice.categories
);

export const getCategoriesMap=createSelector(
    [selectCategories],
    (category)=>category.reduce((acc, data) => {
        const {title, items}= data;
        acc[title.toLowerCase()]=items;
        return acc;
    },{})
);

export const IsCategoriesLoading = createSelector(
    [selectCategoryReducer],
    (categorySlice) => categorySlice.isLoading
);

// export const getCategoriesMap=(state)=> {
//     const categoryMap=  state.categories.categories.reduce((acc, data) => {
//             const {title, items}= data;
//             acc[title.toLowerCase()]=items;
//             return acc;
//         },{});
//      return  categoryMap;
// }