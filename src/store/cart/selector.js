import { createSelector } from "reselect";

 const selectCart=(state)=> state.cart;

 export const selectCartItems=createSelector(
    [selectCart],
    (cart)=>cart.cartItems
 );

export const selectCartItemCount=createSelector(
    [selectCartItems],
    (cartItems)=>cartItems.reduce((total,cartItem)=> total+cartItem.quantity,0)
)

export const selectCartItemTotal=createSelector(
    [selectCartItems],
    (cartItems)=>cartItems.reduce((total,cartItem)=> total+cartItem.quantity * cartItem.price,0)
)

export const selectIsCartOpen=createSelector(
    [selectCart],
    (cart)=>cart.isCartOpen
)