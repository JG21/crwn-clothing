import { createAction } from "../../utils/reducer";
import { CART_ACTION_TYPE } from "./types";


const addCartItem=(cartItems,productToAdd)=>{
    const isItemPresent=cartItems.find(cartItem=> cartItem.id === productToAdd.id)
    
    return ( isItemPresent ? cartItems.map(cartItem=>cartItem.id===productToAdd.id ? {...cartItem, quantity: cartItem.quantity+1 } : cartItem) : [...cartItems, {...productToAdd, quantity:1}])
};

const removeCartItem=(cartItems,cartItemToRemove)=>{
    const isItemPresent=cartItems.find(cartItem=> cartItem.id === cartItemToRemove.id)
    
   return( isItemPresent.quantity <=1 ? cartItems.filter(cartItem=>cartItem.id!==cartItemToRemove.id) : cartItems.map(cartItem=>cartItem.id===cartItemToRemove.id ? {...cartItem, quantity: cartItem.quantity-1 }: cartItem)); 
}

const clearCartItem=(cartItems,cartItemToClear )=>{
 return cartItems.filter(item=>item.id!==cartItemToClear.id);
}

export const addItemToCart =(cartItems,productToAdd)=>{
    const newCartItems=addCartItem(cartItems,productToAdd);
   return createAction(CART_ACTION_TYPE.SET_CART_ITEM, newCartItems)
    //updateCartItemReducer(newCartItems);
    
}
export const removeItemFromCart =(cartItems,cartItemToRemove)=>{
    const newCartItems=removeCartItem(cartItems,cartItemToRemove);
   return createAction(CART_ACTION_TYPE.SET_CART_ITEM, newCartItems)
    //updateCartItemReducer(newCartItems);
   
}
export const clearItemfromCart =(cartItems,cartItemToClear)=>{
    const newCartItems=clearCartItem(cartItems,cartItemToClear);
    return createAction(CART_ACTION_TYPE.SET_CART_ITEM, newCartItems)
    //updateCartItemReducer(newCartItems);
     
}

export const setIsCartOpen=(bool)=>createAction(CART_ACTION_TYPE.CART_OPEN_TOGGLE, bool);
  