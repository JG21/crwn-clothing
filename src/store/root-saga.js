import { all, call } from 'redux-saga/effects';
import { categoriesSaga } from '../store/categories/saga'
import { userSaga } from '../store/user/saga';

export function* rootSaga() {
    yield all([call(categoriesSaga), call(userSaga)])
}